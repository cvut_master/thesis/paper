.PHONY: all

all: compile detex

compile:
	latexmk -synctex=1 -interaction=nonstopmode -file-line-error -pdf -aux-directory=out -output-directory=out out/thesis.pdf

detex:
	opendetex thesis.tex > out/thesis.txt
	opendetex thesis-content.tex > out/thesis-content.txt
